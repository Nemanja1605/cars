#pragma once

#include <SDL.h>
#include <SDL_image.h>


class Texture {
public:
    Texture(SDL_Texture *texture = nullptr, int width = 0, int height = 0);

    ~Texture();

    bool load(const char *path, SDL_Renderer *renderer);

    void render(int x, int y,
                SDL_Renderer *renderer,
                SDL_Rect *clip = nullptr,
                double angle = 0.0,
                SDL_Point *center = nullptr,
                SDL_RendererFlip flip = SDL_FLIP_NONE);

    int getWidth();

    int getHeight();

private:
    SDL_Texture *texture;
    int width;
    int height;
};
#pragma once

#include "drawable.h"
#include "movable.h"
#include "sprite.h"

class Vehicle: public Drawable, public Movable {
public:
    Vehicle(Sprite *sprite);

    virtual void draw(SDL_Renderer *renderer);

    virtual void move();

    virtual void move(int x, int y);

    SDL_Rect *getSpriteRect();

    int getSpeed();

    void setSpeed(int speed);
private:
    Sprite *sprite;
    int speed = 10;
};

#include <random>
#include "engine.h"
#include "sprite.h"
#include "texture.h"
#include "player.h"
#include "vehicle.h"

Engine::Engine(std::string title) {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(),
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              640, 480, 0
            /*SDL_WINDOW_RESIZABLE*/);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void loadTexture(const char *path, Texture *texture, SDL_Renderer *renderer) {
    bool loaded = texture->load(path, renderer);
    if (!loaded) {
        std::cout << "Nije moguce otvoriti datoteku: " << path << std::endl;
    }
}

int random(int min, int max) {
    return (int) (rand() % max + min);
}

Vehicle *createVehicle(Texture *texture, SDL_Renderer *renderer) {
    Sprite *sp = new Sprite(texture, renderer);
    Vehicle *vh = new Vehicle(sp);
    int halfHeight = texture->getHeight() / 2;
    vh->move(640 + random(0, 3000), random(halfHeight, 480) - halfHeight);
    vh->setSpeed(random(5, 10));
    return vh;
}

void Engine::run() {
    bool isRunning = true;
    int maxDelay = 1000 / 60, frameStart, frameEnd;
    SDL_Event event;

    // Background
    Texture *backgroundTexture = new Texture();
    loadTexture("../resources/background.png", backgroundTexture, renderer);
    int backgroundOffset = 0;
    int speed = 10;

    // Player
    Texture *playerTexture = new Texture();
    loadTexture("../resources/cars/audi.png", playerTexture, renderer);
    Sprite *playerSprite = new Sprite(playerTexture, renderer);
    Player *player = new Player(playerSprite);
    player->move(50, 240 - playerTexture->getHeight() / 2);
    movables.push_back(player);
    eventListeners.push_back(player);

    // Vehicles
    std::vector<Vehicle *> vehicles;
    Texture *vehicleTexture = new Texture();
    loadTexture("../resources/cars/black_viper.png", vehicleTexture, renderer);

    srand(time(NULL));
    for (int i = 0; i < 10; i++) {
        Vehicle *vh = createVehicle(vehicleTexture, renderer);
        vehicles.push_back(vh);
        drawables.push_back(vh);
        movables.push_back(vh);
    }

    while (isRunning) {
        frameStart = SDL_GetTicks();
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                isRunning = false;
            } else {
                for (int i = 0; i < eventListeners.size(); ++i) {
                    eventListeners[i]->listen(event);
                }
            }
        }

        for (auto &movable : movables) {
            movable->move();
        }

        for (auto it = vehicles.begin(); it < vehicles.end(); ++it) {
            SDL_Rect *vehicleRect = (*it)->getSpriteRect();

            if (vehicleRect->x < 0) {
                it = vehicles.erase(it);
                Vehicle *vh = createVehicle(vehicleTexture, renderer);
                vehicles.push_back(vh);
                drawables.push_back(vh);
                movables.push_back(vh);
            } else {
                // Collision
                SDL_Rect *playerRect = player->getSpriteRect();
                if (playerRect->x < vehicleRect->x + vehicleRect->w &&
                    playerRect->x + playerRect->w > vehicleRect->x &&
                    playerRect->y < vehicleRect->y + vehicleRect->h &&
                    playerRect->y + playerRect->h > vehicleRect->y) {
                    isRunning = false;
                }
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        backgroundOffset -= speed;
        if (backgroundOffset < -backgroundTexture->getWidth()) {
            backgroundOffset = 0;
        }

        backgroundTexture->render(backgroundOffset, 0, renderer);
        backgroundTexture->render(backgroundOffset + backgroundTexture->getWidth(), 0, renderer);
        player->draw(renderer);

        for (auto &drawable : drawables) {
            drawable->draw(renderer);
        }

        SDL_RenderPresent(renderer);

        frameEnd = SDL_GetTicks();
        if (frameEnd - frameStart < maxDelay) SDL_Delay(maxDelay - (frameEnd - frameStart));
    }
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

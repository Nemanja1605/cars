#include "engine.h"

int main() {
    Engine *engine = new Engine("Cars");
    engine->run();
    delete engine;

    return 0;
}

#include "player.h"

Player::Player(Sprite *sprite) {
    this->sprite = sprite;
}

void Player::draw(SDL_Renderer *renderer) {
    sprite->draw(renderer);
}

void Player::move() {
    switch (sprite->getState()) {
        case Sprite::UP:
            move(0, -10);
            break;
        case Sprite::DOWN:
            move(0, 10);
            break;
        case Sprite::STOP:
            break;
    }
}

void Player::move(int x, int y) {
    sprite->move(x, y);
}

void Player::listen(SDL_KeyboardEvent &event) {
    if (event.type == SDL_KEYDOWN) {
        if (event.keysym.sym == SDLK_UP) {
            sprite->setState(Sprite::UP);
        } else if (event.keysym.sym == SDLK_DOWN) {
            sprite->setState(Sprite::DOWN);
        }
    } else if (event.type == SDL_KEYUP) {
        sprite->setState(Sprite::STOP);
    }
}

SDL_Rect *Player::getSpriteRect() {
    return sprite->getSpriteRect();
}

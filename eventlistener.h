#pragma once

#include <SDL_events.h>

class EventListener {
public:
    virtual void listen(SDL_Event &event) = 0;
};
#pragma once

#include "sprite.h"
#include "keyboardeventlistener.h"

class Player : public Drawable, public Movable, public KeyboardEventListener {
public:
    Player(Sprite *sprite);

    virtual void draw(SDL_Renderer *renderer);

    virtual void move();

    virtual void move(int x, int y);

    virtual void listen(SDL_KeyboardEvent &event);

    SDL_Rect *getSpriteRect();
private:
    Sprite *sprite;
    int speed = 50;
};
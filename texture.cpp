#include "texture.h"

Texture::Texture(SDL_Texture *texture, int width, int height)
        : texture(texture), width(width), height(height) {
}

Texture::~Texture() {
    SDL_DestroyTexture(texture);
    texture = nullptr;
    width = 0;
    height = 0;
}

bool Texture::load(const char *path, SDL_Renderer *renderer) {
    SDL_Surface *surface = IMG_Load(path);
    if (surface == nullptr) return false;

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == nullptr) return false;

    width = surface->w;
    height = surface->h;

    SDL_FreeSurface(surface);

    return true;
}

void Texture::render(int x, int y, SDL_Renderer *renderer, SDL_Rect *clip, double angle, SDL_Point *center,
                     SDL_RendererFlip flip) {
    SDL_Rect *rect = new SDL_Rect{x, y, width, height};
    if (clip != nullptr) {
        rect->w = clip->w;
        rect->h = clip->h;
    }
    SDL_RenderCopyEx(renderer, texture, clip, rect, angle, center, flip);
    delete rect;
}

int Texture::getWidth() {
    return width;
}

int Texture::getHeight() {
    return height;
}

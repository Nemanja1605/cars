#include "vehicle.h"

Vehicle::Vehicle(Sprite *sprite) {
    this->sprite = sprite;
}

void Vehicle::draw(SDL_Renderer *renderer) {
    sprite->draw(renderer);
}

void Vehicle::move() {
    sprite->move(-speed, 0);
}

void Vehicle::move(int x, int y) {
    sprite->move(x, y);
}

SDL_Rect *Vehicle::getSpriteRect() {
    return sprite->getSpriteRect();
}

int Vehicle::getSpeed() {
    return speed;
}

void Vehicle::setSpeed(int speed) {
    this->speed = speed;
}

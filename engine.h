#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <SDL.h>
#include <SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "eventlistener.h"


class Engine {
private:
    SDL_Window *window;
    SDL_Renderer *renderer;

    std::vector<Drawable *> drawables;
    std::vector<Movable *> movables;
    std::vector<EventListener *> eventListeners;
public:
    Engine(std::string title);

    void run();

    virtual ~Engine();
};
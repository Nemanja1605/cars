#pragma once

#include <SDL.h>

class Movable {
public:
    virtual void move() = 0;

    virtual void move(int x, int y) = 0;
};
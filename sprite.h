#pragma once

#include <string>

#include <SDL.h>
#include <SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "texture.h"

class Sprite : public Drawable, public Movable {
public:
    enum State : short int {
        STOP = 0,
        UP = 1,
        DOWN = 2
    };

    Sprite(Texture *texture, SDL_Renderer *renderer);

    State getState();

    void setState(State state);

    virtual void draw(SDL_Renderer *renderer);

    virtual void draw(SDL_Renderer *renderer, double angle);

    virtual void move();

    virtual void move(int x, int y);

    SDL_Rect *getSpriteRect();

private:
    State state;
    SDL_Rect *spriteRect;
    Texture *texture;
};
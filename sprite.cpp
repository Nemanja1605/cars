#include "sprite.h"

Sprite::Sprite(Texture *texture, SDL_Renderer *renderer) {
    state = Sprite::STOP;
    this->texture = texture;
    spriteRect = new SDL_Rect{
            .x = 0,
            .y = 0,
            .w = this->texture->getWidth(),
            .h = this->texture->getHeight()
    };
}

Sprite::State Sprite::getState() {
    return state;
}

void Sprite::setState(Sprite::State state) {
    this->state = state;
}

void Sprite::draw(SDL_Renderer *renderer) {
    texture->render(spriteRect->x, spriteRect->y, renderer);
}

void Sprite::draw(SDL_Renderer *renderer, double angle) {
    texture->render(spriteRect->x, spriteRect->y, renderer, nullptr, angle);

}

void Sprite::move() {
    switch (state) {
        case UP:
            move(0, -1);
            break;
        case DOWN:
            move(0, 1);
            break;
        case STOP:
            break;
    }
}

void Sprite::move(int x, int y) {
    spriteRect->x += x;
    spriteRect->y += y;
}

SDL_Rect *Sprite::getSpriteRect() {
    return spriteRect;
}
